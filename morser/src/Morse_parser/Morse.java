﻿package Morse_parser;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Morse{

	private static final String[] signal = {"\u30FB\uFF0D","\u30FB\uFF0D\u30FB\uFF0D","\uFF0D\u30FB\u30FB\u30FB","\uFF0D\u30FB\uFF0D\u30FB","\uFF0D\u30FB\u30FB","\u30FB","\u30FB\u30FB\uFF0D\u30FB\u30FB","\u30FB\u30FB\uFF0D\u30FB","\uFF0D\uFF0D\u30FB","\u30FB\u30FB\u30FB\u30FB","\uFF0D\u30FB\uFF0D\uFF0D\u30FB","\u30FB\uFF0D\uFF0D\uFF0D","\uFF0D\u30FB\uFF0D","\u30FB\uFF0D\u30FB\u30FB","\uFF0D\uFF0D","\uFF0D\u30FB","\uFF0D\uFF0D\uFF0D","\uFF0D\uFF0D\uFF0D\u30FB","\u30FB\uFF0D\uFF0D\u30FB","\uFF0D\uFF0D\u30FB\uFF0D","\u30FB\uFF0D\u30FB","\u30FB\u30FB\u30FB","\uFF0D","\u30FB\u30FB\uFF0D","\u30FB\uFF0D\u30FB\u30FB\uFF0D","\u30FB\u30FB\uFF0D\uFF0D","\u30FB\uFF0D\u30FB\u30FB\u30FB","\u30FB\u30FB\u30FB\uFF0D","\u30FB\uFF0D\uFF0D","\uFF0D\u30FB\u30FB\uFF0D","\uFF0D\u30FB\uFF0D\uFF0D","\uFF0D\uFF0D\u30FB\u30FB","\uFF0D\uFF0D\uFF0D\uFF0D","\uFF0D\u30FB\uFF0D\uFF0D\uFF0D","\u30FB\uFF0D\u30FB\uFF0D\uFF0D","\uFF0D\uFF0D\u30FB\uFF0D\uFF0D","\uFF0D\u30FB\uFF0D\u30FB\uFF0D","\uFF0D\u30FB\uFF0D\u30FB\u30FB","\uFF0D\u30FB\u30FB\uFF0D\uFF0D","\uFF0D\u30FB\u30FB\u30FB\uFF0D","\u30FB\u30FB\uFF0D\u30FB\uFF0D","\uFF0D\uFF0D\u30FB\uFF0D\u30FB","\u30FB\uFF0D\uFF0D\u30FB\u30FB","\uFF0D\uFF0D\u30FB\u30FB\uFF0D","\uFF0D\u30FB\u30FB\uFF0D\u30FB","\u30FB\uFF0D\uFF0D\uFF0D\u30FB","\uFF0D\uFF0D\uFF0D\u30FB\uFF0D","\u30FB\uFF0D\u30FB\uFF0D\u30FB","\u30FB\u30FB","\u30FB\u30FB\uFF0D\uFF0D\u30FB","\u30FB\uFF0D\uFF0D\uFF0D\uFF0D","\u30FB\u30FB\uFF0D\uFF0D\uFF0D","\u30FB\u30FB\u30FB\uFF0D\uFF0D","\u30FB\u30FB\u30FB\u30FB\uFF0D","\u30FB\u30FB\u30FB\u30FB\u30FB","\uFF0D\u30FB\u30FB\u30FB\u30FB","\uFF0D\uFF0D\u30FB\u30FB\u30FB","\uFF0D\uFF0D\uFF0D\u30FB\u30FB","\uFF0D\uFF0D\uFF0D\uFF0D\u30FB","\uFF0D\uFF0D\uFF0D\uFF0D\uFF0D","\u30FB\uFF0D\uFF0D\u30FB\uFF0D","\u30FB\uFF0D\u30FB\uFF0D\u30FB\uFF0D","\u30FB\uFF0D\u30FB\uFF0D\u30FB\u30FB","\uFF0D\u30FB\uFF0D\uFF0D\u30FB\uFF0D","\u30FB\uFF0D\u30FB\u30FB\uFF0D\u30FB"};
	private static final String[] compil_kana = {"イ","ロ","ハ","ニ","ホ","ヘ","ト","チ","リ","ヌ","ル","ヲ","ワ","カ","ヨ","タ","レ","ソ","ツ","ネ","ナ","ラ","ム","ウ","ヰ","ノ","オ","ク","ヤ","マ","ケ","フ","コ","エ","テ","ア","サ","キ","ユ","メ","ミ","シ","ヱ","ヒ","モ","セ","ス","ン","゛","゜","1","2","3","4","5","6","7","8","9","0","ー","、","\n","（"," ）"};
	private static final String[] compil_hira = {"い","ろ","は","に","ほ","へ","と","ち","り","ぬ","る","を","わ","か","よ","た","れ","そ","つ","ね","な","ら","む","う","ヰ","の","お","く","や","ま","け","ふ","こ","え","て","あ","さ","き","ゆ","め","み","し","ヱ","ひ","も","せ","す","ん","゛","゜","1","2","3","4","5","6","7","8","9","0","ー","、","\n","（"," ）"};
	private static final String[] daku_kana = {"ガ","ギ","グ","ゲ","ゴ","ザ","ジ","ズ","ゼ","ゾ","ダ","ヂ","ヅ","デ","ド","バ","ビ","ブ","ベ","ボ","パ","ピ","プ","ペ","ポ"};
	private static final String[] daku_hira = {"が","ぎ","ぐ","げ","ご","ざ","じ","ず","ぜ","ぞ","だ","ぢ","づ","で","ど","ば","び","ぶ","べ","ぼ","ぱ","ぴ","ぷ","ぺ","ぽ"};
	private static final String[] daku_nasi = {"カ゛","キ゛","ク゛","ケ゛","コ゛","サ゛","シ゛","ス゛","セ゛","ソ゛","た゛","チ゛","ツ゛","テ゛","ト゛","ハ゛","ヒ゛","フ゛","ヘ゛","ホ゛","ハ゜","ヒ゜","フ゜","ヘ゜","ホ゜"};
	private static final String pattern_text = "(((\u30FB|\uFF0D)+) ?)+";
	private static final Pattern pattern = Pattern.compile(pattern_text, Pattern.CASE_INSENSITIVE);
	
	public static String create(String text){
		
		for(int i=0;i<daku_kana.length;i++){
			text=text.replaceAll(daku_kana[i], daku_nasi[i]);
			text=text.replaceAll(daku_hira[i], daku_nasi[i]);
		}
		
		
		for(int i=0;i<signal.length;i++){
			text=text.replace(compil_kana[i], signal[i]+" ");
			text=text.replace(compil_hira[i], signal[i]+" ");
		}
		return text;
	}
	
	public static String parser(String text){
		
		if(text==null || text.length()==0)return text;

        text = text.replace('\u002D','\uFF0D');
		Matcher matcher = pattern.matcher(text);
		
		if(matcher.groupCount()==0)return text;
		
		ArrayList<String> list=new ArrayList<String>();
		String ress;
		while(matcher.find()){
			String m=matcher.group();
			String[] s_m = m.split(" ");
			if(s_m.length>1){
				ress="";
				for(int j=0;j<s_m.length;j++){
					for(int i=0;i<signal.length;i++){
						if(s_m[j].equals(signal[i])){
							ress+=compil_kana[i];
							break;
						}
					}
				}
				list.add("(\""+ress+"\")");
			}
		}
		for(String s:list)text=text.replaceFirst(pattern_text,s);

		return text;
	}

}