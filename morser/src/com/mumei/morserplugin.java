package com.mumei;

import java.awt.Component;
import java.util.Date;

import javax.swing.JPanel;
import javax.swing.JTextPane;
import twitter4j.Status;
import twitter4j.URLEntity;
import Morse_parser.Morse;
import com.orekyuu.javatter.plugin.*;
import com.orekyuu.javatter.util.SaveData;
import com.orekyuu.javatter.view.IJavatterTab;

public class morserplugin extends JavatterPlugin{
	
	private final String TAG = "[morser plugin]";
	private SaveData data;

	@Override
	public String getPluginName() {
		// TODO Auto-generated method stub
		return "morser plugin";
	}

	@Override
	public String getVersion() {
		// TODO Auto-generated method stub
		return "1.0";
	}
	
	

	@Override
	public void init() {
		Log("Active");

		data = getSaveData();
		data.setDefaultValue("isReadMorse", true);
		addTweetObjectBuider(builder);
	}

	@Override
	protected IJavatterTab getPluginConfigViewObserver() {
		return  new IJavatterTab(){
			@Override
			public Component getComponent() {
				return new SettingWindow(getMainView().getTweetTextArea(),data);
			}
			
		};
	}

	private void Log(String text){
		System.out.println("["+new Date()+"]:"+TAG+text);
	}
	
	TweetObjectBuilder builder = new TweetObjectBuilder(){

		@Override
		public void createdButtonPanel(JPanel arg0, Status arg1) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void createdImagePanel(JPanel arg0, Status arg1) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void createdObjectPanel(JPanel arg0, Status arg1) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void createdTextAreaPanel(JPanel panel, Status status) {
			
			if(data.getBoolean("isReadMorse"))for(int i=0;i<panel.getComponentCount();i++){
				Component comp = panel.getComponent(i);
				if(comp instanceof JTextPane){
					JTextPane text = (JTextPane)comp;
					text.setText(Morse.parser( createShowText(status) ));
					break;
				}
			}
			
		}
		
	};
	
	private String createShowText(Status status){
		Status showStatus = status.isRetweet()?status.getRetweetedStatus():status;
		
		String text = showStatus.getText();
		
		URLEntity[] urls = showStatus.getURLEntities();
		
		for(URLEntity url:urls){
			String expanded = url.getExpandedURL();
			text = text.replace(url.getURL(),"<a href=\""+expanded+"\">"+(expanded.length()>30?expanded.substring(0,30)+"...":expanded)+"</a>");
		}
		
		urls = showStatus.getMediaEntities();

		for(URLEntity url:urls){
			String expanded = url.getExpandedURL();
			text = text.replace(url.getURL(),"<a href=\""+expanded+"\">"+(expanded.length()>30?expanded.substring(0,30)+"...":expanded)+"</a>");
		}
		
		return text.replaceAll("\n", "<br>");
	}
}
