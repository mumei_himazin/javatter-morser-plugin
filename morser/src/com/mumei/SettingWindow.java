package com.mumei;

import java.awt.Dimension;
import java.awt.Window;

import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import Morse_parser.Morse;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JCheckBox;

import com.orekyuu.javatter.util.SaveData;

public class SettingWindow extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1112186532337345821L;
	
	public SettingWindow(final JTextArea tweetArea,final SaveData data){
		
		setLayout(null);
		setPreferredSize(new Dimension(360, 360));
		JLabel label = new JLabel("\u30E2\u30FC\u30EB\u30B9\u4FE1\u53F7");
		label.setBounds(12, 10, 146, 13);
		add(label);
		
		
		final JTextArea show = new JTextArea();
		show.setColumns(5);
		show.setLineWrap(true);
		show.setEnabled(false);
		show.setEditable(false);
		show.setBounds(12, 33, 336, 135);
		add(show);
		
		final JTextArea edit = new JTextArea();
		edit.setColumns(5);
		edit.setLineWrap(true);
		edit.setBounds(12, 184, 336, 135);
		edit.getDocument().addDocumentListener(new DocumentListener(){
			@Override
			public void insertUpdate(DocumentEvent e) {
				show.setText(Morse.create(edit.getText()));
			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				show.setText(Morse.create(edit.getText()));
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				show.setText(Morse.create(edit.getText()));
			}
		});
		add(edit);

		JButton btnNewButton = new JButton("\u5165\u529B");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tweetArea.append(show.getText());
				Window w = SwingUtilities.getWindowAncestor(SettingWindow.this);
				w.setVisible(false);
				
			}
		});
		btnNewButton.setBounds(257, 329, 91, 21);
		add(btnNewButton);
		
		final JCheckBox checkBox = new JCheckBox("\u30E2\u30FC\u30EB\u30B9\u3092\u89E3\u8AAD\u3059\u308B");
		checkBox.setBounds(12, 329, 146, 21);
		checkBox.setSelected(data.getBoolean("isReadMorse"));
		checkBox.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				data.setBoolean("isReadMorse", checkBox.isSelected());
			}
			
		});
		add(checkBox);
		
	}
}
